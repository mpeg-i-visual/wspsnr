cmake_minimum_required(VERSION 3.5 FATAL_ERROR)

set( CMAKE_CXX_STANDARD 11)

project("WS-PSNR")


add_executable(WS-PSNR WS-PSNR/main.cpp WS-PSNR/JsonParser.cpp WS-PSNR/Config.cpp)
