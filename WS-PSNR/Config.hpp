/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2019, ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef _CONFIG_HPP_
#define _CONFIG_HPP_

#include "JsonParser.hpp"

#include <string>
#include <vector>


/**
@file Config.hpp
\brief The file containing the configuration
*/

class Config {
public:
	/** Load configuration from file */
	static Config loadFromFile(std::string const& filename);

	/** Version of the configuration file */
	std::string version;

	/** filenames of the input original images */
	std::string original_file;

	/** filenames of the input reconstructed images */
	std::string reconstructed_file;

  /** Projection type:  Equirectangular=0; Perspective=1 */
  int projection;

  /** Resolution */
  int width;
  int height;

  /** Color space:   8 or 10 */
  int bit_depth;

  /** Number of frames to process */
  int number_of_frames;

  /** peak value of 10 bits:  1020 or 1023 */
  int peak_value_10bits = 1020;

	/** First frame to process for original and reconstructed file (zero-based) */
  long long start_frame_ori = 0;
  long long start_frame_rec = 0;

  /** Longitude and latitude range of ERP, shall be align with video resolution */
  double longRange = 360;
  double latRange = 180;

  /** Valid rectangular region from image boundary, value shall be even */
  int valid_rectangular_region_from_left_boundary_In_luma = 0;
  int valid_rectangular_region_from_right_boundary_In_luma = 0;
  int valid_rectangular_region_from_top_boundary_In_luma = 0;
  int valid_rectangular_region_from_bottom_boundary_In_luma= 0;

private:

  void setVersionFrom(json::Node root);
  void setProjectionType(json::Node root);

  void setOriginalFilepath(json::Node root);
  void setReconstrutedFilepath(json::Node root);
  void setColorSpace(json::Node root);

  void setVideoWidth(json::Node root);
  void setVideoHeight(json::Node root);
  void setBitDepth(json::Node root);
  void setNumberOfFrames(json::Node root);

  void setPeakValue10Bits(json::Node root);

  void setStartFrameOriginalFile(json::Node root);
  void setStartFrameReconstructedlFile(json::Node root);

  void setLongitudeRangeERP(json::Node root);
  void setLatitudeRangeERP(json::Node root);

  void setValidRectangularFromLeftBoundaryInLuma(json::Node root);
  void setValidRectangularFromRightBoundaryInLuma(json::Node root);
  void setValidRectangularFromTopBoundaryInLuma(json::Node root);
  void setValidRectangularFromBottomBoundaryInLuma(json::Node root);

};

#endif