/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2019, ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "Config.hpp"
#include "JsonParser.hpp"

#include <fstream>
#include <iostream>


 Config Config::loadFromFile(std::string const& filename)
{
	std::ifstream stream(filename);
	auto root = json::Node::readFrom(stream);

	Config config;

    config.setVersionFrom(root);
    config.setProjectionType(root);
    config.setOriginalFilepath(root);
    config.setReconstrutedFilepath(root);
	config.setColorSpace(root);
    config.setVideoWidth(root);
    config.setVideoHeight(root);
	config.setBitDepth(root);
    config.setNumberOfFrames(root);
  
    config.setPeakValue10Bits(root);
  
    config.setStartFrameOriginalFile(root);
    config.setStartFrameReconstructedlFile(root);
  
    config.setLongitudeRangeERP(root);
    config.setLatitudeRangeERP(root);
  
    config.setValidRectangularFromLeftBoundaryInLuma(root);
    config.setValidRectangularFromRightBoundaryInLuma(root);
    config.setValidRectangularFromTopBoundaryInLuma(root);
    config.setValidRectangularFromBottomBoundaryInLuma(root);

	return config;
}

void Config::setVersionFrom(json::Node root)
{
	version = root.require("Version").asString();
	if (version.substr(0, 2) != "2.") {
		throw std::runtime_error("Configuration file does not match the WS-PSNR version");
	}
	std::cout << "Version: " << version << '\n';
}


void Config::setProjectionType(json::Node root)
{
	auto node = root.require("Projection");
	if (node) {
		if (node.asString() == "Equirectangular") {
			projection = 0;
			std::cout << "Projection: Equirectangular"<< '\n';
		}
		else if (node.asString() == "Perspective") {
			projection = 1;
			std::cout << "Projection: Perspective" << '\n';
		}
		else {
			throw std::runtime_error("Current software only supports equirectangular and perspective projection");
		}
	}
}

void Config::setOriginalFilepath(json::Node root)
{
	auto node = root.require("Original_file_path");
	if (node) {
		original_file= node.asString();
	}
	std::cout << "Original file path: " << original_file << '\n';
}

void Config::setReconstrutedFilepath(json::Node root)
{
	auto node = root.require("Reconstructed_file_path");
	if (node) {
		reconstructed_file = node.asString();
		std::cout << "Reconstructed file path: " << reconstructed_file << '\n';
	}

}

void Config::setColorSpace(json::Node root)
{
	auto node = root.optional("ColorSpace");
	if (node) {
		if (node.asString() == "YUV420") {
			std::cout << "ColorSpace: YUV420\n";
		}
		else {
			throw std::runtime_error("Current software only supports YUV420\n");
		}
	}
}

void Config::setVideoWidth(json::Node root)
{
  auto node = root.require("Video_width");
  if (node) {
    width = node.asInt();
    std::cout << "Video width: " << width << '\n';
  }
}

void Config::setVideoHeight(json::Node root)
{
  auto node = root.require("Video_height");
  if (node) {
    height = node.asInt();
    std::cout << "Video height: " << height << '\n';
  }
}

void Config::setBitDepth(json::Node root)
{
	auto node = root.require("BitDepth");
	if (node) {
		bit_depth = node.asInt();
		std::cout << "BitDepth: " << bit_depth << '\n';
		if (bit_depth!=8 && bit_depth != 10)
		  throw std::runtime_error("Current software only supports 8 or 10 bit depth\n");
	}
}

void Config::setNumberOfFrames(json::Node root)
{
	auto node = root.require("NumberOfFrames");
	if (node) {
		number_of_frames = node.asInt();
		std::cout << "NumberOfFrames: " << number_of_frames << '\n';
	}
}

void Config::setStartFrameOriginalFile(json::Node root)
{
	auto node = root.optional("Start_frame_of_original_file");
	if (node) {
		start_frame_ori = node.asInt();
		std::cout << "Start frame of original file: " << start_frame_ori << '\n';
	}
}

void Config::setStartFrameReconstructedlFile(json::Node root)
{
	auto node = root.optional("Start_frame_of_reconstructed_file");
	if (node) {
		start_frame_rec = node.asInt();
		std::cout << "Start frame of reconstructed file: " << start_frame_rec << '\n';
	}
}

void Config::setPeakValue10Bits(json::Node root)
{
  auto node = root.optional("Peak_value_of_10bits");
  if (node) {
    peak_value_10bits = node.asInt();
    std::cout << "Peak value of 10bits: " << peak_value_10bits << '\n';
	if (peak_value_10bits != 1020 && peak_value_10bits != 1023)
		throw std::runtime_error("Peak value of 10bits can only be 1020 or 1023\n");
  }
}


void Config::setLongitudeRangeERP(json::Node root)
{
  auto node = root.optional("Longitude_range_of_ERP");
  if (node) {
    longRange = node.asDouble();
    std::cout << "Longitude range of ERP: " << longRange << '\n';
	if (longRange > 360 || longRange <= 0)
		throw std::runtime_error("The longitude range of the ERP must in (0, 360]\n");
  }
}

void Config::setLatitudeRangeERP(json::Node root)
{
  auto node = root.optional("Latitude_range_of_ERP");
  if (node) {
    latRange = node.asDouble();
    std::cout << "Latitude range of ERP: " << latRange << '\n';
	if (latRange > 360 || latRange <= 0)
		throw std::runtime_error("The latitude range of the ERP must in (0, 180]\n");
  }
}

void Config::setValidRectangularFromLeftBoundaryInLuma(json::Node root)
{
  auto node = root.optional("Valid_rectangular_region_from_left_boundary_In_luma");
  if (node) {
    valid_rectangular_region_from_left_boundary_In_luma = node.asInt();
    std::cout << "Valid rectangular region from left boundary in luma: " << valid_rectangular_region_from_left_boundary_In_luma << '\n';
    if(valid_rectangular_region_from_left_boundary_In_luma % 2 == 1)
     {
		throw std::runtime_error("The value of valid rectangular region from left boundary in luma shall be even \n");
     }
  }
}

void Config::setValidRectangularFromRightBoundaryInLuma(json::Node root)
{
  auto node = root.optional("Valid_rectangular_region_from_right_boundary_In_luma");
  if (node) {
    valid_rectangular_region_from_right_boundary_In_luma = node.asInt();
    std::cout << "Valid rectangular region from right boundary in luma: " << valid_rectangular_region_from_right_boundary_In_luma << '\n';
    if(valid_rectangular_region_from_right_boundary_In_luma % 2 == 1)
    {
		throw std::runtime_error("The value of valid rectangular region from right boundary in luma shall be even\n");
    }
  }
}

void Config::setValidRectangularFromTopBoundaryInLuma(json::Node root)
{
  auto node = root.optional("Valid_rectangular_region_from_top_boundary_In_luma");
  if (node) {
    valid_rectangular_region_from_top_boundary_In_luma = node.asInt();
    std::cout << "Valid rectangular region from top boundary in luma: " << valid_rectangular_region_from_top_boundary_In_luma  << '\n';
    if(valid_rectangular_region_from_top_boundary_In_luma  % 2 == 1)
    {
		throw std::runtime_error("The value of valid rectangular region from top boundary in luma shall be even\n");
    }
  }
}

void Config::setValidRectangularFromBottomBoundaryInLuma(json::Node root)
{
  auto node = root.optional("Valid_rectangular_region_from_bottom_boundary_In_luma");
  if (node) {
    valid_rectangular_region_from_bottom_boundary_In_luma = node.asInt();
    std::cout << "Valid rectangular region from bottom boundary in luma: " << valid_rectangular_region_from_bottom_boundary_In_luma << '\n';
    if(valid_rectangular_region_from_bottom_boundary_In_luma % 2 == 1)
    {
		throw std::runtime_error("The value of valid rectangular region from bottom boundary in luma shall be even\n");
    }
  }
}